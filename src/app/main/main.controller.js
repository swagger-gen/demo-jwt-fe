export class MainController {
  constructor($rootScope) {
    'ngInject';
    this.$rootScope = $rootScope;
  }

  get token() {
    return this.$rootScope.jwtToken;
  }

  get formatToken() {
    // return atob(this.$rootScope.jwtToken);
    if (!this.$rootScope.jwtToken) {
      return '';
    }
    var token = this.$rootScope.jwtToken;
    var t = token.split('.');
    var head = atob(t[0]);
    var body = (atob(t[1]));
    var exp = angular.fromJson(body).exp;
    var iat = angular.fromJson(body).iat;
    return 'Header:' + head + '<br/>Body:' + body + '<br/>Issued: ' + new Date(iat * 1000).toLocaleString() + '<br/>Expired: ' + new Date(exp * 1000).toLocaleString();
  }

  get resp() {
    if (!this.$rootScope.resp) {
      return '';
    }
    var s = 'First name: ' + this.$rootScope.resp.name.first;
    s += '<br/>Last name: ' + this.$rootScope.resp.name.last;
    s += '<br/>Login time: ' + this.$rootScope.resp.loginTime;
    s += '<br/>Birthdate: ' + this.$rootScope.resp.birthDate;
    s += '<br/>Success: ' + this.$rootScope.resp.success;

    var bin=atob(this.$rootScope.resp.data);
    var i = 0, l = bin.length, chr, hex = ''
    for (i; i < l; ++i) {
      chr = bin.charCodeAt(i).toString(16)
      hex += chr.length < 2 ? '0' + chr : chr
    }

    s += '<br/>Data: ' + hex;
    return s;
  }

}
