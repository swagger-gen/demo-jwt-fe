angular.module('demo-jwt-fe').factory('authInterceptor', authInterceptor);

authInterceptor.$inject = ['$rootScope', '$log', '$q'];

function authInterceptor($rootScope, $log, $q) {

  return {
    request: function (config) {
      if (config.url.indexOf('demo-jwt') !== -1) {
        config.headers = config.headers || {};
        if ($rootScope.jwtToken) {
          config.headers.Authorization = 'Bearer ' + $rootScope.jwtToken;
        }

        $log.debug('authInterceptor request:');
        $log.debug(config);
      }
      return config;
    },

    response: function (response) {
      return response;
    },

    responseError: function (rejection) {
      if (rejection.status === 401) {
        $log.debug('401 kezelése...');
      }
      return $q.reject(rejection);
    }

  }

}

