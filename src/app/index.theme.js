export function themeConfig ($mdThemingProvider) {
  'ngInject';
  $mdThemingProvider.theme('default')
    .primaryPalette('light-green');
    //.accentPalette('orange');
}
