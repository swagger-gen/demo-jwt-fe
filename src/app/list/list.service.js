angular.module('demo-jwt-fe').factory('ListRS', ListRS);

ListRS.$inject = ['$resource', 'apiUrl'];

function ListRS($resource, apiUrl) {

  return $resource(apiUrl + '/users', {},
    {
      'get': {
        method: 'get',
        isArray: true
      }
    });

}
