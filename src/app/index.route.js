export function routerConfig($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/main/main.html',
      controller: 'MainController',
      controllerAs: 'main'
    })
    .state('login', {
      url: '/',
      templateUrl: 'app/login/login.html',
      controller: 'LoginController',
      controllerAs: 'vm'
    })
    .state('list', {
      url: '/',
      templateUrl: 'app/list/list.html',
      controller: 'ListController',
      controllerAs: 'vm'
    });

  $urlRouterProvider.otherwise('/');
}
