angular.module('demo-jwt-fe').factory('LoginRS', LoginRS);

LoginRS.$inject = ['$resource', 'apiUrl'];

function LoginRS($resource, apiUrl) {

  return $resource(apiUrl + '/login', {},
    {
      'login': { method: 'POST' }
    });
}
