export class LoginController {
  constructor(LoginRS, $mdDialog, $rootScope) {
    'ngInject';
    this.$mdDialog = $mdDialog;
    this.LoginRS = LoginRS;
    this.$rootScope = $rootScope;
    this.user = {
      username: 'user',
      password: 'pass'
    };
  }

  register() {
    this.LoginRS.login(
      this.user,
      (resp) => {
        this.$rootScope.jwtToken=resp.token;
        this.$rootScope.resp=resp;

        var alert=this.$mdDialog.alert({
          title:'Siker',
          textContent: resp,
          ok: 'OK'
        });

        this.$mdDialog
          .show(alert)
          .finally(function () {
            alert=undefined;
          });
      }
    );
  }
}
