import {config} from './index.config';
import {routerConfig} from './index.route';
import {authConfig} from './index.auth';
import {runBlock} from './index.run';
import {MainController} from './main/main.controller';
import {LoginController} from './login/login.controller';
import {ListController} from './list/list.controller';
import {NavbarDirective} from '../app/components/navbar/navbar.directive';
import {themeConfig} from './index.theme';

angular.module('demo-jwt-fe', ['ngSanitize', 'ngMessages', 'ngResource', 'ui.router', 'ngMaterial', 'md.data.table'])
  .config(config)
  .config(routerConfig)
  .config(themeConfig)
  .config(authConfig)
  .run(runBlock)
  .controller('MainController', MainController)
  .controller('LoginController', LoginController)
  .controller('ListController', ListController)
  .directive('acmeNavbar', NavbarDirective)
  .value('apiUrl', 'http://localhost:8080/demo-jwt-1.0.0');
