export class ListController {
  constructor(ListRS) {
    'ngInject';
    var vm = this;
    var errorUser = [{id: 'Bejelentkezési hiba', name: ''}];

    vm.users = [];

    ListRS.get(
      function success(response) {
        vm.users = response;
      },
      function err(error) {
        if (error.status == 401) {
          vm.users = errorUser;

          vm.users[0].name = error.data.message;
        }
      });

  }
}
